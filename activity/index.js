/* Writing Comments in Javascript*/
// one line comments
/* Multi Line Comments */

/*console.log("Hello Batch 144!")*/

/*For Javascript we can see our log message in our console.

Browser Consoles are part of our browsers which will allow us to see/log messages, data or information from our programming language - Javascript.

For most browser, consoles are easily accessed through its developer tools in the console tab.

In fact, console is browser that will allow us to add some Javascript expression.


statements are instructions, expressions that we add to our programming language to communicate with our computers.



JS Statements usually ends with semi colon, however, JS has implemented a way to automatically add semicolons at the end of a line/statement. Semi Colons can actually be omitted in creating JS statements but semicolons in JS are added to mark the end of the stement.

Syntax in programming is a set of rules that describes how statements are properly made/constructed.

Line/blocks of code must follow a certain of rules for it to work properly.



*/

// console.log("Donald");


/*Mini ACtivity*/
/*
console.log("Adobo");
console.log("Adobo");
console.log("Adobo");
*/



let food1 = "Adobo";

console.log(food1);

/*Variables are a way to store information or data within our JS.


To create a variable we first declare the name of the variable with either let/cons keyword:


let nameOfVariable



Then, we can initilialize the variable with a value or data.


let nameOfVariable = <data>;





*/



console.log("My Favorite food is:" + food1);

let food2 = "Kare Kare";

console.log("My Favorite foods are: " + food1 + " and " + food2);

let food3;

/*we can create a variable without add an initial value yet, however, the variable's content is undefined.*/

console.log(food3);


food3 = "Chickenjoy";

/*We can update the content of a let variable by reassigning the content using an assignemnt operator (=)

assignement operator (=) = lets us assign data to a variable

*/

console.log(food3);

/*Mini activity*/

food1 = "Sinigang";
food2 = "Fried Chicken"

console.log(food1, food2);

/*we can update our variables with an assignment operator without needing to use the let keyword again

We cannot create another variable with the same name. It will result in an error.
*/

/*let food1 = "Flat Tops";*/

//const keyword


/*
	coonst keyword will also allow us to create variable. however, with a constant variables, which means these data to saved in a constant will not change, cannot be changed and should not be changed.

*/

const pi = 3.1416;

console.log(pi);
/* Trying to re-assign variable will result into an error.

pi = "Pizza";

console.log(pie);
*/
/*



const gravity;

console.log(gravity);*/

// we also cannot declare/create a const variable without initial value.

/*Mini activity*/

let myName;
const sunriseDirection = "East";
const sunsetDirection = "West";


console.log(myName, sunriseDirection, sunsetDirection);

/*
	Guides in creating JS variable:

	1. We can create a let variable with the let keyword. let variables can be reassign but not redeclared.

	2. Creating a variable has two parts: Declaration of the name and Initialization of the initial value of the variable using an assignment operator (=).

	3. A let variable can be declared without initialization. However the value of the variable will be undefined until it is re-assigned with a value.

	4. Not defined vs Undefined. Not Defined error means the variable is used but not declared. undefined results from a variable that is used but is not initialized.


	5. We can use const keyword to create constant variable. Constant variable cannot be declared without initialization.
	Constant variables cannot be re-assigned.

	6. When creating variable names, start with small caps, this is because of avoiding conflict with syntax in JS that is named with capital letters.

	7. If the variable would need two words, the naming convention is the use of camelCase. Do not add a space for variables with words as names.

*/

/*Data Types

strings ("")are data which are alphanumerical text. It could be a nme, a phrase or even a sentence. We can create string with a single quote or double quote.

variable = "string data";


*/


console.log("Sample String Data");

let country = "Philippines";
let province ="Luzon";



console.log(country, province)

/*
concatenation is a way for us to add strings together and have a single string. By adding + symbol


*/

let fullAddress = province + ", " + country;

console.log(fullAddress);

let greeting = "I live in " + country;

console.log(greeting);

/*in JS, when you use the + sign with strings we have concatenation*/

let numstring = "50";
let numstring2 = "25";

console.log(numstring + numstring2);

/*strings have property called .length and it tells us the number of characters in string.

spaces, commas, periods can also characters when included in a string.

.length will return a number type data

*/
let hero = "Captain America";

console.log(hero.length);

/*Number Type*/

/*
Number type data can actually be used in proper mathematical equation
*/

let students = 16;
console.log(students);

let num1 = 50;
let num2 = 25;

/*
Addition Operator will allow us to add two number types and return the sum as a number data type.

Operations return a value and that value can be saved in a variable.

What if we use the addition operator on a number type and a numerical string?

console.log(num1 + Number(numstring3));

parseInt() will allow us to turn a numeric string into a proper number type.

*/


let sum1 = num1 + num2;
console.log(num1+num2);

console.log(sum1);

let numstring3 = "100";


let sum2 = parseInt(numstring3) + num1;

/*console.log(num1+numstring3);*/
console.log(sum2);

/*const name = "Jeff";

console.log(name, sunsetDirection, sunriseDirection);

console.log(num1, numstring3);*/

/*Mini Activity*/

let sum3 = sum1 + sum2;

let sum4 = parseInt(numstring2) + num2;


console.log(sum3, sum4);


/*

Subtraction operator

will let us get the difference between two number types. It will also result to a proper number type data.

When substraction operator is used on a string and a number, the string will be converted into a number automatically and then JS will perform the operation.

This automatic convertion from one type to another is called type convertion or type coercion or forced coercion.

When a text string is substracted with a number, it will result in Nan or Not A Number. when JS converted the text string it results to NaN and Nan = Nan

*/


console.log(num1-num2);

let difference = num1 - num2;
let difference2 = numstring3 - num2;
console.log(difference2);


let difference3 = hero - num2;

console.log(difference3);

let string1 = "fifteen";

console.log(num2 - string1);

/*

Multiplication Operator

*/


let num3 = 10;
let num4 = 5;
let product = num3 * num4;
console.log(product);

let product2 = numstring3 + num3;
console.log(product2);


let product3 = numstring * numstring2;
console.log(product3);


/*Division Operator (/*/

let num5 = 30;
let num6 = 3;

let quotient = num5 / num6;
console.log(quotient);
let quotient2 = numstring3 / 5;
console.log(quotient2);

let quotient3 = 450/num4;

console.log(quotient3);





/*Boolean (true or false*/
/*Boolean is usually for logical operations or for if-else condition*/
/*Naming convention for a variable that contains boolean is a yes or no question.*/

let isAdmin = true;
let isMarried = false;

/* 

You can actually

*/

//Arrays
//Arrays are a special kind of data type wherein we can store multiple values.
//An array are can store multiple values and even of different types. For best practise, keep the data type of items in an array uniform.

//values in array are separated by comma. Failing to do so, will result in an error.

//An array is created with an Array Literal.





let koponanNiEugene = ["Eugene", "Alfred", "Dennis", "Vincent"];
console.log(koponanNiEugene);


//Array indices are markers of the order of the items in the array. Array index starts at 0
 console.log(koponanNiEugene[0]);

let array2 = ["One Punch Man", true, 500, "Saitama"];

console.log(array2);

/*Objects are another kind of special data types used to mimic real world objects.
With this we can add information that makesa sense, thematically relevant end of different data types.

Objects can be created with Object Literals ({})

Each value is given a label which makes the data significant and meaningful this pairing of data and "label" is what we call a Key-value pair.
A key-value pair together is called a property.

Each property is separated by a comma.
*/

let person1 = {

heroName: "One Punch Man",
isRegistered: true,
salary: 500,
realName: "Saitama"

};

console.log(person1.heroName);


/*
	Mini-Activity
	
*/

let myFavoriteBands = ["Duran Duran", "One Direction", "Marron 5"];

let me = {

	firstName: "Donald",
	lasName: "Morales",
	isWebDeveloper: true,
	hasPortfolio: true,
	/*Properties of an object should relate to each other and thematically relevant to describe a single item/object*/
	age: 34, 

}

/*Undefined vs NUull*/
/*Null
	is the explicit declaration that there is no value.
*/

let sampleNull = null;

/*Undefined
	means that the variable exists, however a value was not initialized with the variable.

*/




let undefinedSample;

console.log(undefinedSample);

//Certain processes in programming explicitly returns null to indicate that task resulted to nothing.

let foundResult = null;

//for undefined, this is normally caused by developers creating variables that have no value or data is associated with them.
//The variable does exist but its value is still unknown.

let person2 = {
	name: "Patricia",
	age: 28
};

console.log(person2.isAdmin)













console.log(myFavoriteBands);
console.log(me.hasPortfolio);
















































































